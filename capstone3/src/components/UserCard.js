import { useState } from 'react';
import { Card, Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserProfile from '../pages/UserProfile';


export default function UserCard({ userProp }) {

	//const { email } = infoProp
    const { email, isAdmin } = userProp
	//console.log(userProp)
	return (

		<Table striped bordered hover>
			  <thead>
			    <tr>
			      <th>User Iformation</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{email}</td>
				  <td>{isAdmin}</td>

			    </tr>
			   
			    
			  </tbody>
			</Table>
	)
}