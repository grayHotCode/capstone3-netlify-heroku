import {Fragment} from 'react';
import Banner from '../components/Banner';

 



export default function Home(){

	const data={
		title: "Welcome to Jamir Fruit Store ",
	 	content: "It’s all about fruits",
	 	destination:"/products",
	 	label: "Order Now!"
 }

	 return(
	 	<Fragment>
	 		<Banner data={data} />
	 	</Fragment>


	 	)
}